#!/usr/bin/python

class student(object):
	
	@property
	def birth(self):
		return self._birth

# give the birth setter property	
	@birth.setter
	def birth(self,value):
		self._birth = value
	
#	only can read
	@property	
	def age(self):
		return  10 - self._birth
	
a = student()
a.birth = 2
print(a.birth)
print(a.age)