while True:
	try:
		x = raw_input().split()
		m = int(x[0])
		n = int(x[1])
		for i in range(0,m):
			a = []
			row = raw_input().split()
			for num in row:
				a.append(int(num))
			Sum = sum(a)
			MAX = max(a)
			j = 0
			while a[j] != MAX:
				j = j + 1
			a[j] = Sum
			print(' '.join(str(y) for y in a))
	except EOFError:
			break