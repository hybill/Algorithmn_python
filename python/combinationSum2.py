#!/usr/bin/python
class Solution(object):
	def combinationSum2(self, candidates, target):
		"""
		:type candidates: List[int]
		:type target: int
		:rtype: List[List[int]]
		"""
		rs = []
		candidates.sort()
		self.DFS([],candidates,target,0,rs)
		return rs
	
	def DFS(self,path,nums,target,index,rs):
		if target == 0:
			rs.append(path)
		elif target > 0:
			for i in xrange(index,len(nums)):
				if i -1 >= index and nums[i] == nums[i-1]:continue
				self.DFS(path+[nums[i]],nums,target-nums[i],i+1,rs)
			
a = Solution()
s = a.combinationSum2([10, 1, 2, 7, 6, 1, 5], 8)
print s