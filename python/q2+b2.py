#!/usr/bin/python
class Solution(object):
	def judgeSquareSum(self, c):
		"""
		:type c: int
		:rtype: bool
		"""
		def isquare(n):
			return True if int(n**0.5)**2 == n else False
		i = 0
		Sq = 0
		while Sq <= c:
			print Sq
			if isquare(c-Sq):
				return True
			else:
				i += 1
				Sq = i**2
		return False
		
a =Solution()
print a.judgeSquareSum(11)