#!/usr/bin/python

class Solution:
	# @param {integer[]} nums
	# @return {string}
	def largestNumber(self, nums):
		nums.sort(lambda x,y:cmp(int(str(x)+str(y)), int(str(y)+str(x))))
		return ''.join(map(str, nums)[::-1])

a = Solution()
a.largestNumber([3, 30, 34, 5, 9])