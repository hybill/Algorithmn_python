#!/usr/bin/python

class Solution(object):
	def uniqueMorseRepresentations(self, words):
		"""
		:type words: List[str]
		:rtype: int
		"""
		table = [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]
		ans = set()
		for s in words:
			st = ''
			for i in s:
				st += table[ord(i) - 97]
			ans.add(st)
		return len(ans)

words = ["gin", "zen", "gig", "msg"]

a = Solution()
print a.uniqueMorseRepresentations(words)			