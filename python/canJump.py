#!/usr/bin/python
class Solution(object):
	def canJump(self, nums):
		p1 = 0
		p2 = 0
		l = len(nums)
		while -1 <p1 <= p2 and p2 < l-1:
			a = nums[p2]
			if a != 0:
				p2 = p2 +a
				p1 = p2 -1
			else:
				b = nums[p1]
				if p2 < p1+b:
					p2 = p1 + b
					p1 = p2 - 1
				else:
					p1 -= 1
		if p2 >= l-1:
			return True
		else:
			return False

a = Solution()
print a.canJump([5,9,3,2,1,0,2,3,3,1,0,0])

