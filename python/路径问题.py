#!/usr/bin/python
class Solution(object):
	def minPathSum(self, grid):
		"""
		:type grid: List[List[int]]
		:rtype: int
		"""
		for i in range(0,len(grid)):
			for j in range(0,len(grid[0])):
				if i > 0 and j >0:
					grid[i][j] = grid[i][j] + min(grid[i][j-1],grid[i-1][j])
				elif i == 0 and j > 0:
					grid[i][j] = grid[i][j] + grid[i][j-1]
				elif i>0 and j == 0:
					grid[i][j] = grid[i][j] + grid[i-1][j]
		print grid
		return grid[len(grid)-1][len(grid[0])-1]

c = Solution()
print c.minPathSum([[1,2],[1,1]])
