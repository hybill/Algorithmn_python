#!/usr/bin/python
class Solution(object):
	def calculate(self, s):
		"""
		:type s: str
		:rtype: int
		"""
		if s.isdigit():
			return int(s)
		num = 0
		stack = []
		sign = '+'
		for i in s:
			if i.isdigit():
				num = num *10 + ord(i)-ord('0')
			if not i.isdigit() and not i.isspace():
				if sign == '+':
					stack.append(num)
				elif sign == '-':
					stack.append(-num)
				elif sign == '*':
					stack.append(stack.pop() * num)
				elif sign == '/':
					a = stack.pop()
					if a >0:
						stack.append(a/num)
					else:
						stack.append(abs(a)/num*-1)
				sign = i
				num = 0
		if sign == '+':
			stack.append(num)
		elif sign == '-':
			stack.append(-num)
		elif sign == '*':
			stack.append(stack.pop() * num)
		elif sign == '/':
			a = stack.pop()
			if a >0:
				stack.append(a/num)
			else:
				stack.append(abs(a)/num*-1)      
		return sum(stack)

a = Solution()
print a.calculate('14-3/2')
