#!/usr/bin/python
try:
	while True:
		n,m = map(int,raw_input().split(' '))
		print(bin(n).replace('0b', ''))
		print(bin(m).replace('0b', ''))
		print(bin(n^m).replace('0b', ''))
except IOError:
	print error