#!/usr/bin/python
class Solution(object):
	def PredictTheWinner(self, nums):
		def MinMax(deep,s,left,right):
			if right == left:
				return s
			if deep%2 == 1:
				return min(MinMax(deep+1, s-nums[left], left+1, right),MinMax(deep+1, s-nums[right], left, right-1))
			else:
				return max(MinMax(deep+1, s+nums[left], left+1, right),MinMax(deep+1, s+nums[right], left, right-1))
		if MinMax(0, 0, 0, len(nums)-1) < 0:
			return False
		else:
			return True
		
a = Solution()
print a.PredictTheWinner([1, 5, 233, 7])