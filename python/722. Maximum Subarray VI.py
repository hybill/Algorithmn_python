#!/usr/bin/python
class Solution:
	"""
	@param nums: the array
	@return: the max xor sum of the subarray in a given array
	"""
	def maxXorSubarray(self, nums):
		# write code here
		while True:
			a = []
			while nums:
				p = nums.pop()
				if nums and p^nums[-1] > max(p,nums[-1]):
					a.append(p^nums[-1])
					nums.pop()
				else:
					a.append(p)		
			if len(a) == len(nums):
				pass
			else:
				nums = a
			print nums
		return max(nums)
			
			


s = Solution()
print s.maxXorSubarray([8, 1, 2, 12, 7, 6])
					
			