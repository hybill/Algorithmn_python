# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
	def constructMaximumBinaryTree(self, nums):
		"""
		:type nums: List[int]
		:rtype: TreeNode
		"""
		Tree = TreeNode(None)
		def D(root,nums):
			if not nums:
				return
			maxValue = max(nums)
			n = nums.index(maxValue)
			root.val = maxValue
			if nums[:n]:
				Tree.left = TreeNode(None)
				D(Tree.left,nums[:n])
			if nums[n+1:]:
				Tree.right = TreeNode(None)
				D(Tree.right,nums[n+1:])
		D(Tree,nums)	
		return Tree	
			