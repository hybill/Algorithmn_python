import java.util.*;

/*
public class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
    public TreeNode(int val) {
        this.val = val;
    }
}*/
public class Checker {
    public boolean checkBST(TreeNode root) {
        // write code here
    int min = Integer.MIN_VALUE;
    int max = Integer.MAX_VALUE;
    return  check(root,min,max);

    }
    public boolean check(TreeNode root,int min,int max){
    if (root == null){
    	return true;
    }
    if (root.val > max || root.val < min){
    	return false;
    }
    if (!check(root.left,min,root.val) || !check(root.right,root.val,max)){
         return false;
    }
    return true;
    }
}