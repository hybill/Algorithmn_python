#!/usr/bin/python

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
	def pruneTree(self, root):
		"""
		:type root: TreeNode
		:rtype: TreeNode
		"""
		if not root:
			return None
		head = self.subTree(root)
		if head:
			return root
		else:
			return None
	def subTree(self,node):
		if not node:
			return False
		left = self.subTree(node.left)
		right = self.subTree(node.right)
		if not left:
			node.left = None
		if not right:
			node.right = None
		if node.val == 1:
			return 1
		else:	
			return left or right