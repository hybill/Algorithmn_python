#!/usr/bin/python
class Solution(object):
	def happy(self,n):
		s = 0
		while n>0:
			a = n % 10
			n = n //10
			s = s + a**2
		if s == 1:
			return True
		if s in self.a:
			return False
		else:
			self.a.add(s)
		return self.happy(s)
	def isHappy(self, n):
		"""
		:type n: int
		:rtype: bool
		"""
		self.a = set()
		return self.happy(n)

s = Solution()
print s.isHappy(13)
print s.a