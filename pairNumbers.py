#!/usr/bin/python
class Solution:
	"""
	@param p: the point List
	@return: the numbers of pairs which meet the requirements
	"""
	def pairNumbers(self, p):
		# Write your code here
		TT = 0
		TF = 0
		FT = 0
		FF = 0
		for i in p:
			if i.x%2 == 0:
				if i.y%2 == 0:
					TT += 1
				else:
					TF += 1
			else:
				if i.y%2 == 0:
					FT += 1
				else:
					FF += 1
		return TT*(TT-1)/2 + FF*(FF-1)/2
		
a = Solution()
print a.pairNumbers([[0,3],[1,1],[3,4],[5,6]])