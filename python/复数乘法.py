#!/usr/bin/python

class ComplexNumberMultiplication(object):
	def action(self):
		try:
			a = raw_input()
			b = raw_input()
			x1,y1 = map(int,a.replace('i', '').split('+'))
			x2,y2 = map(int,b.replace('i', '').split('+'))
			x = x1 * x2 - y1*y2
			y = x1 * y2 + x2 *y1
			print str(x)+'+'+str(y)+'i'
		except EOFError:
			print EOFError

a = ComplexNumberMultiplication()
a.action()