class Solution:
	def TopK(self, tinput, k):
		# write code here
		if k == 0:
			return input(0)
		mid = tinput[0]
		right = []
		left = []
		for i in tinput[1:]:
			if mid < i:
				left.append(i)
			else:
				right.append(i)
		if k < len(left):
			return self.GetLeastNumbers_Solution(left, k)
		elif k == len(left)+1:
			return mid
		else:
			return self.GetLeastNumbers_Solution(right, k-len(left)-1)