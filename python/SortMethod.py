#!/usr/bin/python

class solution(object):
	def MergeSort(self,nums):
		mid = len(nums)//2
		left = nums[:mid]
		right = nums[mid:]
		if len(left)>1:
			left = self.MergeSort(left)
		if len(right)>1:
			right = self.MergeSort(right)
		rs = []
		while left and right:
			if left[-1] > right[-1]:
				rs.append(left.pop())
			else:
				rs.append(right.pop())
		rs.reverse()
		return (left or right)+rs
	
	def QuickSort(self,nums):
		if not nums:
			return []
		left = []
		right = []
		mid = nums[0]
		for i in nums[1::]:
			if i < mid:
				left.append(i)
			else:
				right.append(i)
		left = self.QuickSort(left)
		right = self.QuickSort(right)
		return left+[mid]+right
				
		
		
		
		
		
nums = [1,42,544,3,2,3,42,5,4,3,23,4,5,6]
a = solution()
print a.MergeSort(nums)
print a.QuickSort(nums)
