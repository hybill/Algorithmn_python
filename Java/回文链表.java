import java.util.*;

/*
public class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}*/
public class Palindrome {
    public boolean isPalindrome(ListNode pHead) {
        // write code here
        Stack<ListNode> half = new Stack();
        ListNode fast = pHead;
        ListNode slow = pHead;
        if (pHead == null || pHead.next == null ){
        	return true;
        }
        while(fast.next != null && fast.next.next != null){
        	fast = fast.next.next;
        	slow = slow.next;
        	half.push(slow);
        }
        if (fast.next != null){
        	slow = slow.next;
        }
        ListNode cur = slow;
        while (cur.next != null){
        	if (cur.val != half.pop().val){
        		return  false;
        	}
        	cur = cur.next;

        }
        return true;
    }
}