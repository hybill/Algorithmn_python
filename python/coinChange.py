#!/usr/bin/python
class Solution(object):
	def coinChange(self, coins, amount):
		"""
		:type coins: List[int]
		:type amount: int
		:rtype: int
		"""
		import sys
		dp = [sys.maxint]*(amount+1)
		dp[0] = 0
		for i in range(1,amount+1):
			for j in range(len(coins)):
				a = coins[j]
				if a<= i:
					dp[i] = min(dp[i],dp[i-a]+1)
		if dp[amount]>amount:
			return -1
		else:
			return dp[amount]
			

		
			