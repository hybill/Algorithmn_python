#!/usr/bin/python

class Solution(object):
	def combine(self, n, k):
		"""
		:type n: int
		:type k: int
		:rtype: List[List[int]]
		"""
		rs = []
		self.DFS([],0,k,rs,n)
		return rs
	
	def DFS(self,path,index,k,rs,n):
		if k == 0:
			rs.append(path)
		elif k >0:
			for i in xrange(index,n):
				self.DFS(path+[i+1], i+1, k-1, rs, n)

a = Solution()
print a.combine(4, 2)