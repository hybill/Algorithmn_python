#!/usr/bin/python
class Solution(object):
	def isMatch(self, s, p):
		length = len(s)
		dp = [True] + length * [False]
		if len(p) - p.count('*') > length:
			return False
		for i in p:
			if i != '*':
				for n in reversed(range(length)):
					dp[n+1] = dp[n] and (i == s[n] or i== '?')
			else:
				for n in range(1,length+1):
					dp[n] = dp[n-1] or dp[n]
			dp[0] = dp[0] and i == '*'
		return dp[-1]
			
		


a = Solution()
print a.isMatch('aabaaabbbc', 'a*?')
print a.isMatch('aaa', 'a')
print a.isMatch('ab', '?*')
