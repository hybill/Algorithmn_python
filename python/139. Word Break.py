#!/usr/bin/python

class Solution(object):
	def wordBreak(self, s, wordDict):
		"""
		:type s: str
		:type wordDict: List[str]
		:rtype: bool
		"""
		rs = [False]*len(s)
		for i in xrange(len(s)):
			for w in wordDict:
				if w == s[i-len(w)+1:i+1] and (rs[i-len(w)] or i - len(w) == -1):
					rs[i] = True
		return rs[-1]

s = 'leetcode'
words=['le','leet','code']
word_break(s, words)