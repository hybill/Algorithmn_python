#!/usr/bin/python

class Solution(object):
	def moveZeroes(self, nums):
		"""
		:type nums: List[int]
		:rtype: void Do not return anything, modify nums in-place instead.
		"""
		if not nums:
			return []
		l = len(nums)
		if l == 1:
			return nums
		left = 0
		for i in range(l):
			if nums[i] != 0 and i!= left:
				nums[left] = nums[i]
				nums[i] = 0
				left += 1
		print nums


a = Solution()
a.moveZeroes([0])