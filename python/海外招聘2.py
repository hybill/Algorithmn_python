#!/usr/bin/python

n = int(raw_input())
rs = map(float, raw_input().split(' '))
times = int(raw_input())
areas = [i**2 for i in rs]
for i in xrange(n-1,0,-1):
	areas[i] = areas[i] - areas[i-1]
d = sum(areas)
areas = [i/d for i in areas]
score = 1.0
Sum = 0
for i in areas[::-1]:
	Sum += i * score
	score += 1
print("%.3f" % (Sum*times))

	