class Solution(object):
	def numberOfLines(self, widths, S):
		"""
		:type widths: List[int]
		:type S: str
		:rtype: List[int]
		"""
		if not S:
			return [0,0]
		line = 1
		empty = 100
		for i in S:
			index = ord(i)-97
			if empty >= widths[index]:
				empty -= widths[index]
			else:
				line += 1
				empty = 100 - widths[index]
		return [line,100-empty]

widths = [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
S = "abcdefghijklmnopqrstuvwxyz"
a = Solution()
print a.numberOfLines(widths, S)