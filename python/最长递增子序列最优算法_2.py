#!/usr/bin/python

a = [2,1,5,3,6,4,8,9,7] 

def LIS(nums):
	if not nums:
		return []
	B = [nums[0]]
	for num in nums:
		if num > B[-1]:
			B.append(num)
		else:
			index = binarySearch(B, num)
			B[index] = num
		print(B)
	return len(B)
		
		
def binarySearch(nums,k):
	left = 0
	right = len(nums) - 1
	while left <= right:
		mid = (left+right)//2
		if k < nums[mid]:
			right = mid - 1
		elif k> nums[mid]:
			left = mid + 1
		else:
			return mid
	return left

print LIS(a)	