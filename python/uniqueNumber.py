#!/usr/bin/python
class Solution(object):
	def countNumbersWithUniqueDigits(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		s = 0
		i = 0
		while i < n:
			p = 9
			x = 9
			j = 0
			while j < i:
				p = p * x
				x -= 1
				j += 1
			s += p
			i += 1
		return s+1

a = Solution()
print a.countNumbersWithUniqueDigits(2)
