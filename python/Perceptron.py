#!/usr/bin/python
from random import choice 
from numpy import array, dot, random
from pylab import plot, ylim
import scipy.stats
import numpy as np
import matplotlib.pyplot as plt
 
class Perceptron:
	def __init__(self,data,e,times,numberOffeatures):
		self.training_data = data
		self.eta = e
		self.n = times
		self.unit_step = lambda x: 0 if x < 0 else 1
		self.w = random.rand(numberOffeatures)
#		print(self.w)
		self.errors = []
	
	def Winsorizing(self,arr,upper,lower):
		for i in range(len(arr)):
			if arr[i]> upper:
				arr[i] = upper
			if arr[i] < lower:
				arr[i] = lower
		return arr
						
	def learning(self):
		for i in xrange(self.n): 
			x, expected = choice(training_data)
			result = dot(self.w, x) 
			error = expected - self.unit_step(result) 
			self.errors.append(error) 
			self.w += self.eta * error * x
		self.resultSet = []
		for x, exceptValue in training_data: 
			result = dot(x, self.w) 
			ouput = self.unit_step(result)
			self.resultSet.append((exceptValue,ouput))
#			print("{}->{}: {} -> {}".format(x,exceptValue, result, ouput))
	
	def learningWithWinsorizing(self,upper,lower):
			for i in xrange(self.n): 
				x, expected = choice(training_data)
				result = dot(self.w, x) 
				error = expected - self.unit_step(result) 
				self.errors.append(error) 
				self.w += self.eta * error * x
			self.resultSet = []
			self.w = self.Winsorizing(self.w, upper, lower)
			for x, exceptValue in training_data: 
				result = dot(x, self.w) 
				ouput = self.unit_step(result)
				self.resultSet.append((exceptValue,ouput))
#				print("{}->{}: {} -> {}".format(x,exceptValue, result, ouput))
	
	def measure(self):
		TP,FP,FN,TN= 0.0,0.0,0.0,0.0
		for (x,y) in self.resultSet:
			if x == 1 and y == 1:
				TP += 1
			elif x == 0 and y == 0:
				TN += 1
			elif x == 1 and y == 0:
				FP += 1
			elif x == 0 and y == 1:
				FN += 1
		P = TP + FP
		N = FN + TN
		ACC = (TP+TN)/(P+N)
		print 'Classification accuracy(ACC) =',ACC
		print 'Classification error =',(FN+FP)/(P+N)
		print 'Sensitivity =',TP/P
		print 'Specificty = ',TN/N
		return ACC
		
				
training_data = [ (array([0,0,1]), 0), 
(array([0,30,1]), 1), 
(array([1,0,1]), 1), 
(array([1,1,1]), 1), 
(array([0,1,0]), 0),
(array([0,0,1]), 0),
(array([1,1,0]), 1)] 


p = Perceptron(training_data, 0.1, 100,3)
p.learning()
y1 = p.measure()


x = []
y = []
for i in range(1,100,5):
	p.learningWithWinsorizing(i/100, -i/100)
	x.append(i*0.01)
	y.append(p.measure())

plt.plot(x,[y1]*len(x), '-')
plt.plot(x,y, '-o')
plt.show()