#!/usr/bin/python

class Solutions(object):
	def combinationSum(self, candidates, target):
		"""
		:type candidates: List[int]
		:type target: int
		:rtype: List[List[int]]
		"""
		rs = []
		candidates.sort()
		self.DFS([],candidates,0,target,rs)
		return rs
	
	def DFS(self,path,nums,index,target,rs):
		if target < 0:
			return
		if target == 0:
			rs.append(path)
			return
		else:
			for i in xrange(index,len(nums)):
				self.DFS(path+[nums[i]], nums,i, target - nums[i], rs)


a = Solutions()
s = a.combinationSum([2, 3, 6, 7], 7)
print(s)