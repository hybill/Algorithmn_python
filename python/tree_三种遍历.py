#!/usr/bin/python
import sys

class TreeNode(object):
	def __init__(self,x):
		self.val = x
		self.left = None
		self.right = None

def put(root,num):
	if root is None:
		return TreeNode(num)
	elif num == root.val:
		return root
	elif num < root.val:
		root.left = put(root.left,num)
		return root
	else :
		root.right = put(root.right, num)
		return root
		
def build(nums):
	root = None
	for num in nums:
		root = put(root, num)
	return root
	
def preoder(root):
	if root is not None:
		sys.stdout.write('%d ' % root.val)
		preoder(root.left)
		preoder(root.right)
		
def inorder(root):
	if root is not None:
		inorder(root.left)
		sys.stdout.write('%d ' % root.val)
		inorder(root.right)
 
 
def postorder(root):
	if root is not None:
		postorder(root.left)
		postorder(root.right)
		sys.stdout.write('%d ' % root.val)
 
while True:
	try:
		input()
		nums = map(int, raw_input().split())
		tree = build(nums)
		preoder(tree)
		print
		inorder(tree)
		print
		postorder(tree)
		print
	except EOFError:
		break

