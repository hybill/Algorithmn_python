class MarkingPositionMonitor:
	def __init__(self):
		self.position = 0
		self.sellorders = {}
		self.buyorders = {}
		self.cancellist = set()
		
	def on_event(self, message):
		import json
		D = json.loads(message)
		if not D.get('type'):
			print('not type error')
		else:
			Type = D['type']
			if Type == 'NEW':
				self.NEW(D)
			elif Type == 'ORDER_ACK':
				self.ORDER_ACK(D)
			elif Type == 'ORDER_REJECT':
				self.ORDER_REJECT(D)
			elif Type == 'CANCEL':
				self.CANCEL(D)
			elif Type == 'CANCEL_ACK':
				self.CANCEL_ACK(D)
			elif Type == 'CANCEL_REJECT':
				self.CANCEL_REJECT(D)
			elif Type == 'FILL':
				self.FILL(D)
		return self.position
			
	def NEW(self,D):
		if D.get('order_id') and D.get('side') and D.get('quantity'):
			if D['side'] == 'BUY':
				self.buyorders[D['order_id']] = int(D['quantity'])
			elif D['side'] == 'SELL':
				sell = int(D['quantity'])
				self.position = self.position - sell
				self.sellorders[D['order_id']] = int(D['quantity'])
					
	def ORDER_ACK(self,D):
		if D.get('order_id'):
			if D['order_id'] in self.sellorders.keys():
				del self.sellorders[D['order_id']]
			if D['order_id'] in self.buyorders.keys():
				if self.position<0 and abs(self.position) > self.buyorders[D['order_id']]:
					self.position += self.buyorders[D['order_id']]
				del self.buyorders[D['order_id']]
			if D['order_id'] in self.cancellist:
				pass
							
	
	def ORDER_REJECT(self,D):
		if D.get('order_id'):
			if D['order_id'] in self.sellorders.keys():
				self.position += self.sellorders[D['order_id']]
				del self.sellorders[D['order_id']]
			if D['order_id'] in self.buyorders.keys():
				del self.buyorders[D['order_id']]
			if D['order_id'] in self.cancellist:
				self.cancellist.remove(D['order_id'])

	
	def CANCEL(self,D):
		if D.get('order_id'):
			if D['order_id'] in self.sellorders.keys():
				self.cancellist.add(D['order_id'])
			if D['order_id'] in self.buyorders.keys():
				self.cancellist.add(D['order_id'])
				
	def CANCEL_ACK(self,D):
		if D.get('order_id'):
			if D['order_id'] in self.cancellist:
				if D['order_id'] in self.sellorders.keys():
					self.position += self.sellorders[D['order_id']]
				if D['order_id'] in self.buyorders.keys():
					self.position -= self.buyorders[D['order_id']]
	
	def CANCEL_REJECT(self,D):
		pass
		
	def FILL(self,D):
		if D.get('filled_quantity'):
			self.position = int(D['filled_quantity']) - sum(self.sellorders.values())
			self.sellorders.clear()
			
a = MarkingPositionMonitor
