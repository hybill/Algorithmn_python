class Solution(object):
	def lastRemaining(self, n):
		return self.LeftToRight(n)
	
	def LeftToRight(self,n):
		if n <= 2:
			return n
		return 2*self.RightToLeft(n/2)
		
	def RightToLeft(self,n):
		if n <= 2:
			return 1
		if n % 2 == 1:
			return 2 * self.LeftToRight(n/2)
		return 2 * self.LeftToRight(n/2) -1
		

a = Solution()
print a.lastRemaining(9)