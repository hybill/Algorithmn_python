#!/usr/bin/python
class Solution(object):
	def findClosestElements(self, arr, k, x):
		"""
		:type arr: List[int]
		:type k: int
		:type x: int
		:rtype: List[int]
		"""
		l = len(arr)
		right = l
		left = 0
		while right-left>1:
			mid = (left+right)>>1
			if x<=arr[mid]:
				right = mid
			else:
				left = mid
		a = 0
		if left+k-1<0:
			a = left
		else:
			a = l-k
		while a > 0 and x - arr[a-1]<= arr[a+k-1]-x:
			a -=1		
		return arr[a:a+k]

a = Solution()
print a.findClosestElements([1,2,3,4,5,6], 4, 3)
print a.findClosestElements([1], 1, 1)
print a.findClosestElements([0,0,0,1,2,3,5,6,7,8,8], 2, 2)				
			