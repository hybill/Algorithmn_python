#!/usr/bin/python
class Solution(object):
	def PredictTheWinner(self, nums):
		l = len(nums)
		DP = [[0 for i in range(l+1)] for j in range(l+1)]
		for i in range(l):
			DP[i][i] = nums[i]
		for i in range(l):
			j = 0
			while i+j < l:
				DP[j][j+i] = max(nums[j+i]-DP[j][j+i-1],nums[j]-DP[j+1][j+i])
				j += 1
		return DP[0][l-1] >= 0
		
a = Solution()
print a.PredictTheWinner([1, 5, 233, 7])