#!/usr/bin/python
class GoUpstairs:
	def countWays(self, n):
		# write code here
		ls = [0 for _ in xrange(n)]
		ls[0] = 1
		ls[1] = 2
		for i in xrange(2,n):
			ls[i] = ls[i-1] + ls[i-2]
		return ls[-1]%1000000007

a = GoUpstairs()
print a.countWays(3)