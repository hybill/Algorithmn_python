#!/usr/bin/python
class Solution(object):
	def minSwap(self, A, B):
		"""
		:type A: List[int]
		:type B: List[int]
		:rtype: int
		"""
		merge = []
		while A and B:
			if A[-1]>B[-1]:
				merge.append(A.pop())
			else:
				merge.append(B.pop())
		if A:
			while A:
				merge.append(A.pop())
		if B:
			while B:
				merge.append(B.pop())
		merge.reverse()
		print(merge)
		s = sorted(merge)
		l = len(s)+1
		dp = [[0] * l for _ in xrange(l)]
		for i in xrange(1,l):
			for j in xrange(1,l):
				if merge[i-1] == s[j-1]:
					dp[i][j] = max(dp[i-1][j-1]+1,dp[i-1][j],dp[i][j-1])
				else:
					dp[i][j] = max(dp[i-1][j],dp[i][j-1])
		return l -1 - dp[-1][-1]

a = Solution()
a.minSwap([1,3,5,4], [1,2,3,7])
