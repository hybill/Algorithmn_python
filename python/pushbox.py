#!/usr/bin/python
try:
	while True:
		n,m = map(int, raw_input().split(' '))
		Map = []
		for i in range(n):
			Map.append(list(raw_input()))
		Ex,Ey,Sx,Sy,Ox,Oy = 0,0,0,0,0,0
		for y in range(n):
			for x in range(m):
				if Map[y][x] == 'E':
					Ex,Ey = x,y
				elif Map[y][x] == '0':
					Ox,Oy = x,y
				elif Map[y][x] == 'S':
					Sx,Sy =x,y			
		State = {}
		def LX(x):
			if 0<=x < m:
				return True
			else:
				return False
		def LY(y):
			if 0 <= y < n:
				return True
			else:
				return False
		def check(Ex,Ey,Sx,Sy,Ox,Oy,x,y):
			if  LX(Sx+x) and LY(Sy+y):
				nextp = Map[Sy+y][Sx+x]
				if nextp == '.' or nextp =='E' or nextp =='S' :
					return Ex,Ey,Sx+x,Sy+y,Ox,Oy,True
				elif nextp == '#':
					return Ex,Ey,Sx,Sy,Ox,Oy,False
				elif nextp == '0':
					if LX(Ox+x) and LY(Oy+y) and Map[Oy+y][Ox+x] != '#':
						return Ex,Ey,Sx+x,Sy+y,Ox+x,Oy+y,True
					else:
						return Ex,Ey,Sx,Sy,Ox,Oy,False
			else:
				return Ex,Ey,Sx,Sy,Ox,Oy,False
				
		def move(Ex,Ey,Sx,Sy,Ox,Oy,step,x,y):
			Ex,Ey,Sx,Sy,Ox,Oy,canmove=check(Ex, Ey, Sx, Sy, Ox, Oy, x, y)
			if canmove:
				if State.get((Sx,Sy,Ox,Oy)):
					if State[(Sx,Sy,Ox,Oy)] > step:
						State[(Sx,Sy,Ox,Oy)] = step
						if Ex != Ox or Ey != Oy:
							move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,-1,0)
							move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,1,0)
							move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,0,-1)
							move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,0,1)
				else:
					State[(Sx,Sy,Ox,Oy)] = step
					if Ex != Ox or Ey != Oy:
						move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,-1,0)
						move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,1,0)
						move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,0,-1)
						move(Ex, Ey, Sx, Sy, Ox, Oy, step+1,0,1)
		move(Ex, Ey, Sx, Sy, Ox, Oy,0, 0, 0)
		rs = []
		for (a,b,c,d),value in State.items():
			if c == Ex and d == Ey:
				rs.append(value)
		if rs:
			print min(rs)
		else:
			print -1
except EOFError:
	print EOFError
