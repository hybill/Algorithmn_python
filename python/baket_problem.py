class Solution(object):
	def isValid(self,s):	
		x = []
		D = {"]":"[", "}":"{", ")":"(","[":"]", "{":"}", "(":")"}
		for i in s[::]:
			if i == '[' or i == ']' or i == ')' or i == '(' or i == '}' or i == '{':
				if len(x) == 0:
					x.append(i)
				else:
					if i == D.get(x[len(x)-1]):
						x.pop()
					else:
						x.append(i)
		if len(x) == 0:
			return True
		else:
			return False


