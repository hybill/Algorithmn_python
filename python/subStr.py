#!/usr/bin/python

class Solution:
	def subStr(self, A, B):
		# write code here
		dp = [[0 for _ in xrange(len(B)+1)] for _ in xrange(len(A)+1)]
		for i in xrange(len(A)):
			for j in xrange(len(B)):
				if A[i] == B[j]:
					dp[i+1][j+1] = max(dp[i+1][j],dp[i][j+1],dp[i][j]+1)
				else:
					dp[i+1][j+1] = max(dp[i+1][j],dp[i][j+1])
		for i in dp:
			print i
		return dp[-1][-1]

A = 'adfag'
B = 'adgs'
a = Solution()
print a.subStr(A, B)