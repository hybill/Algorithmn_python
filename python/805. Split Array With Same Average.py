#!/usr/bin/python
class Solution(object):
	def splitArraySameAverage(self, A):
		"""
		:type A: List[int]
		:rtype: bool
		"""
		ave = float(sum(A))/len(A)
		res = []
		self.dfs(A, ave, 0, [], res)
		print(res)
		if not res:
			return False
		else:
			return True
		
	def dfs(self, nums, ave, index, path, res):
		if ave*len(path)< sum(path):
			return  # backtracking
		if ave*len(path)== sum(path):
			if res:
				res.append(path)
				return 
		for i in xrange(index, len(nums)):
			self.dfs(nums, target-nums[i], i, path+[nums[i]], res)

A = [3,1]
a = Solution()
print a.splitArraySameAverage(A)