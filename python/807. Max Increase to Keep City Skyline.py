#!/usr/bin/python
class Solution(object):
	def maxIncreaseKeepingSkyline(self, grid):
		"""
		:type grid: List[List[int]]
		:rtype: int
		"""
		sum1 = sum([sum(i) for i in grid])
		x = [0] * len(grid)
		y = [0] * len(grid[0])
		for i in xrange(len(grid)):
			for j in xrange(len(grid[0])):
				x[i] = max(x[i],grid[i][j])
				y[j] = max(y[j],grid[i][j])
		for i in xrange(len(grid)):
			for j in xrange(len(grid[0])):
				grid[i][j] = min(x[i],y[j])
		sum2 = sum([sum(i) for i in grid])
		return sum2 - sum1

grid = [[3,0,8,4],[2,4,5,7],[9,2,6,3],[0,3,1,0]]
a = Solution()
print a.maxIncreaseKeepingSkyline(grid)
				
