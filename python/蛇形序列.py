#!/usr/bin/python
class Solution(object):
	def exist(self, board, word):
		"""
		:type board: List[List[str]]
		:type word: str
		:rtype: bool
		"""
		self.l1 = len(board)
		self.l2 = len(board[0])
		self.words = word
		self.wordsl = len(word)
		self.boards = board
		self.map = [[False for i in range(self.l2)] for j in range(self.l1)]
		for i in range(self.l1):
			for j in range(self.l2):
				if self.check(i,j,0):
					return True
		return False
	
	def check(self,x,y,index):
		if index == self.wordsl:
			return True
		if x<0 or x >=self.l1 or y<0 or y >=self.l2:
			return False
		if self.map[x][y]:
			return False
		if self.boards[x][y] == self.words[index]:
			self.map[x][y] = True
			if self.check(x-1,y,index+1) or self.check(x+1,y,index+1) or self.check(x,y-1,index+1) or self.check(x,y+1,index+1):
				return True
			self.map[x][y] = False
			return False

a = Solution()
#print a.exist(["ABCE","BFCS","ADEE"], 'ABCCED')
print a.exist(['a'], 'a')