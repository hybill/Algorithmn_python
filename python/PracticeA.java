import java.util.*;

public  class PracticeA{
	private static List<String> collectSameElements(List<String> collection1, List<String> collection2) {
			//实现练习要求，并改写该行代码。
			List<String> rs = new ArrayList<String>();
			for (int i = 0; i < collection1.size(); i++) {
				for (int j = 0; j < collection2.size(); j++) {
					if (collection1.get(i) == collection2.get(j)){
							rs.add(collection1.get(i));
							break;
					}
				}
			}
			return rs;
		}
	
	public static void main(String[] args) {
		List<String> collection1 = Arrays.asList("a", "e", "h", "t", "f", "c", "g", "b", "d");
		List<String> collection2 = Arrays.asList("a", "d", "e", "f");
		List<String> rs = collectSameElements(collection1, collection2);
		for (int i = 0; i < rs.size(); i++) {
			System.out.print(rs.get(i));
		}
	}
}