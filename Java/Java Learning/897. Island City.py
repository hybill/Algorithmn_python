#!/usr/bin/python
class Solution:
	"""
	@param grid: an integer matrix
	@return: an integer 
	"""
	def numIslandCities(self, grid):
		# Write your code here
		self.x,self.y = len(grid[0]),len(grid)
		self.isVisited = [[False] * self.x for _ in xrange(self.y)]
		self.c = 0
		def search(x0,y0,lock):
			if 0<=x0<self.x and 0<=y0<self.y:
				if self.isVisited[x0][y0] == False:
					self.isVisited[x0][y0] = True
					if grid[x0][y0] == 2:
						if lock == False:
							self.c += 1
							lock = True
						search(x0+1, y0, lock)
						search(x0-1, y0, lock)
						search(x0, y0+1, lock)
						search(x0, y0-1, lock)
					if grid[x0][y0] == 1:
						search(x0+1, y0, lock)
						search(x0-1, y0, lock)
						search(x0, y0+1, lock)
						search(x0, y0-1, lock)				
		
		for i in range(self.x):
			for j in range(self.y):
				if self.isVisited[i][j] == False:
					if grid[i][j] != 0:
						search(i, j, False)	
					self.isVisited[i][j] = True
				print(i,j)
				for line in self.isVisited:
					print line
		return self.c
		
a = Solution()
print a.numIslandCities(
[[1,2,0,2,0],
[0,1,0,0,1],
[0,0,2,1,2],
[0,0,0,0,0],
[0,0,0,0,2]])
