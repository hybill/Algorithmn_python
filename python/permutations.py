#!/usr/bin/python
from itertools import permutations
s,n = raw_input().split(' ')
for x in [''.join(i) for i in sorted(list(permutations(str(s),int(n))))]:
	print x