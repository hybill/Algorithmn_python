#!/usr/bin/python
class Solution(object):
	def wordPattern(self, pattern, str):
		s = pattern
		t = str.split()
		print zip(s,t)
		return len(set(zip(s, t))) == len(set(s)) == len(set(t)) and len(s) == len(t)

a = Solution()
print a.wordPattern('abbab', 'dog cat cat dog cat a a')