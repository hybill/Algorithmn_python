import sys

#最大子序和
def Subsum(nums):
	Maxsum = Cursum = sys.maxint * -1
	for num in nums:
		Cursum = max(Cursum+num,num)
		Maxsum = max(Cursum,Maxsum)
	return Maxsum
#最大子序和 打印序列
def Subseque(nums):
	Maxsum = Cursum = sys.maxint * -1
	Max = []
	Cur = []
	for num in nums:
		if Cursum+ num < num:
			Cur = [num]
			Cursum = num
		else:
			Cur.append(num)
			Cursum += num
		if Maxsum < Cursum:
			Max = Cur
			Maxsum = Cursum
	return Max

#连续递增子序
def Subincease(nums):
	a = [nums[0]]
	Max = [nums[0]]
	for num in nums[1:]:
		if num >= a[-1]:
			a.append(num)
		else:
			if len(a) > len(Max):
				Max = a
			a = [num]
	return Max
#最长递增子序 打印
def Subincease2(nums):
	nums2 = sorted(nums)
	l1 = len(nums)+1
	l2 = len(nums2)+1
	dp = [[0 for _ in range(l2)] for _ in range(l1) ]
	dp2 = [[[] for _ in range(l2)] for _ in range(l1) ]
	for i in range(1,l1):
		for j in range(1,l2):
			if nums[i-1] == nums2[j-1]:
				dp[i][j] = dp[i-1][j-1]+1
				dp2[i][j].extend(dp2[i-1][j-1])
				dp2[i][j].append(nums[i-1])
			else:
				if dp[i-1][j] < dp[i][j-1]:
					dp[i][j] = dp[i][j-1]
					dp2[i][j] = dp2[i][j-1]
				else:
					dp[i][j] = dp[i-1][j]
					dp2[i][j] = dp2[i-1][j]
	return dp2[-1][-1]

#最长递增子序 长度
def Subincease3(nums):
	nums2 = sorted(nums)
	l1 = len(nums)+1
	l2 = len(nums2)+1
	dp = [[0 for _ in range(l2)] for _ in range(l1) ]
	for i in range(1,l1):
		for j in range(1,l2):
			if nums[i-1] == nums2[j-1]:
				dp[i][j] = dp[i-1][j-1]+1
			else:
				dp[i][j] = max(dp[i][j-1],dp[i-1][j])
	return dp[-1][-1]
	
a = [3,-23,4,2,2,4,2,3,3,-34,4,4,45,-56,6,67,23,3]
print(Subsum(a))
print(Subseque(a))
print(Subincease(a))
print(Subincease2(a))
print(Subincease3(a))
	