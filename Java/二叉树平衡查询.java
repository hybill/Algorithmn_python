import java.util.*;

/*
public class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
    public TreeNode(int val) {
        this.val = val;
    }
}*/
public class Balance {
    public boolean isBalance(TreeNode root) {
        // write code here
         int a = checkheight(root);
         if (a == -1){
         	return false;
         }else{
         	return true;
         }
    }
    public int checkheight (TreeNode root){
    	if (root == null){
    		return 0;
    	}
    	int left = checkheight(root.left);
    	if(left == -1){
    		return -1;
    	}
    	int right = checkheight(root.right);
    	if (right == -1){
    			return -1;
    		}

    	if (Math.abs(left - right) > 1){
    		return -1;
    	}else{
    		return Math.max(left,right)+1;
    	}


    }
}
