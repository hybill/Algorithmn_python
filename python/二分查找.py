#!/usr/bin/python
import datetime
def binarySearch(arr):
	if len(arr)> 2:
		mid = len(arr)//2
		left = arr[:mid]
		right = arr[mid:]
		if left[0] > left[-1]:
			return binarySearch(left)
		elif right[0] > right[-1]:
			return binarySearch(right)
		else:
			return min(left[0],right[0])
	else:
		return min(arr)

def search(arr):
	m = arr[0]
	for i in range(len(arr)-1):
		if arr[i]>arr[i+1]:
			m = arr[i+1]
			break
	return m		

try:
#	ns = map(int,raw_input().split(' '))
	ns = [i for i in range(11,100000000)] + [i for i in range(0,10)]
	begin = datetime.datetime.now()
	print binarySearch(ns)
	end = datetime.datetime.now()
	print end-begin
	begin2 = datetime.datetime.now()
	print(min(ns))
	end2 = datetime.datetime.now()
	print end2-begin2
	begin3 = datetime.datetime.now()
	print(search(ns))
	end3 = datetime.datetime.now()
	print end3-begin3
except EOFError:
	print EOFError