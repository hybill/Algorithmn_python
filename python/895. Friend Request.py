#!/usr/bin/python
class Solution:
	"""
	@param ages: The ages
	@return: The answer
	"""
	def friendRequest(self, ages):
		# Write your code here
		l = len(ages)
		c = 0
		ages.sort()
		for i in xrange(l-1):
			for j in xrange(i+1,l): 
				B = ages[i]
				A = ages[j]
				if B <= A/2 +7 or (B < 100 and A >100):
					break
				else:
					c += 1
				if A == B:
					c += 1
		return c
		
a = Solution()
print a.friendRequest([10,44,39,50])
