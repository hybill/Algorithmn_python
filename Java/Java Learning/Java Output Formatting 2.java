import java.util.Scanner;

class Java_Output_Formatting {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int i = scan.nextInt();
		double d = scan.nextDouble();
		scan.nextLine();
		String s = scan.nextLine();

		// Write your code here.

		System.out.println("String:\t " + s);
		System.out.println("Double: \t" + d);
		System.out.println("Int: \t" + i);
	}
}