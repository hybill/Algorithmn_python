#!/usr/bin/python
class Solution(object):
	def combinationSum2(self, candidates, target):
		"""
		:type candidates: List[int]
		:type target: int
		:rtype: List[List[int]]
		"""
		def back(index,candidates,path,curSum,target,rs):
			if curSum == target:
				rs.append(path)
			if curSum < target:
				for i in xrange(index,len(candidates)):
					if i - 1 >= index and  candidates[i-1] == candidates[i]:continue
					back(i+1,candidates,path+[candidates[i]],curSum+candidates[i],target,rs)
		candidates.sort()
		rs = []
		back(0, candidates, [], 0, target,rs)
		return rs
a = Solution()
print a.combinationSum2([10, 1, 2, 7, 6, 1, 5], 8)