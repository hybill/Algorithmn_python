#!/usr/bin/python
class Solution(object):
	def champagneTower(self, poured, query_row, query_glass):
		"""
		:type poured: int
		:type query_row: int
		:type query_glass: int
		:rtype: float
		"""
		
		glass = []
		for i in xrange(1,query_row+2):
			row = [0]*i
			glass.append(row)
		glass[0][0] = float(poured)
		for i in xrange(query_row):
			for j in xrange(len(glass[i])):
				if glass[i][j]>1:
					extra = (glass[i][j] - 1)/2
					glass[i][j] = 1
					glass[i+1][j] += extra
					glass[i+1][j+1] += extra
		return glass[query_row][query_glass] if glass[query_row][query_glass]<=1 else 1.0
		
a = Solution()
print a.champagneTower(2, 1, 1)