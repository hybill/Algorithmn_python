#!/usr/bin/python
class Solution(object):
	def toHex(self, num):
		"""
		:type num: int
		:rtype: str
		"""
		dic = {10:'a',11:'b',12:'c',13:'d',14:'e',15:'f'}
		if num == 0:
				return "0"
		if num < 0:
			num = num + 2**32
		ans = []
		while num > 15:
			a = num%16
			if a <10:
				ans.append(str(a))
			else:
				ans.append(dic[a])
			num = num //16
		if num < 10:
				ans.append(str(num))
		else:
			ans.append(dic[num])
		return ''.join(ans[::-1])

a = Solution()
print a.toHex(16)