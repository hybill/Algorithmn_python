class Solution(object):
	def inorderTraversal(self, root):
		self.ans = []
	        if not root:
	            return []
		def inorder(node):
			if node.left != None:
				inorder(node.left)
			self.ans.append(node.val)
			if node.right != None:
				inorder(node.right)
		inorder(root)
		return self.ans