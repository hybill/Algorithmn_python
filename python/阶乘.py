def f(n):
	if n == 1:
		return 1
	return n*f(n-1)
def x(n):
	if n == 1:
		return 1
	if n == 2:
		return 2
	return f(n)+x(n-2)
while True:
	try:
		n = input()
		if n%2 == 1:
			print(x(n)," ",x(n-1))
		else:
			print(x(n-1)," ",x(n))
	except EOFError:
		break