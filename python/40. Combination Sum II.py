#!/usr/bin/python
class Solution(object):
	def combinationSum(self, candidates, target):
		"""
		:type candidates: List[int]
		:type target: int
		:rtype: List[List[int]]
		"""
		candidates.sort()
		self.rs = set()
		self.back(0, candidates, [], 0, target)
		return [list(i) for i in self.rs]
	def back(self,index,candidates,path,curSum,target):
		if curSum == target:
			self.rs.add(tuple(path))
		if curSum < target:
			for i in xrange(index,len(candidates)):
				self.back(i+1,candidates,path+[candidates[i]],curSum+candidates[i],target)

a = Solution()
print a.combinationSum([10, 1, 2, 7, 6, 1, 5], 8)