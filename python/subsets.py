#!/usr/bin/python
class Solution(object):
	def subsets(self, nums):
		n = 2**len(nums)
		ans = []
		start = 0
		while start < n:
			s = []
			i = 0
			L = list(bin(start)[2:])
			while L:
				a = L.pop()
				if a == '1':
					s.append(nums[i])
				i +=1
			start += 1		
			ans.append(s)	
		return ans	
		
				
		
a = Solution()
print a.subsets([1,2,3,4])