#!/usr/bin/python
import math
class Solution(object):
	def largestTriangleArea(self, points):
		"""
		:type points: List[List[int]]
		:rtype: float
		"""
		l = len(points)
		d = [[0 for _ in xrange(l)] for _ in xrange(l)]
		maxS = 0;
		for x in xrange(l):
			for y in xrange(l):
				d[x][y] = math.sqrt((points[x][0]-points[y][0])**2+(points[x][1]-points[y][1])**2)
		for x in xrange(0,l-2):
			for y in xrange(x+1,l-1):
				for z in xrange(y,l):
					s = self.seq(d[x][y],d[x][z],d[y][z])
					maxS = max(maxS,s)
		return math.sqrt(maxS)
					
	
	def seq(self,a,b,c):
		p = (a+b+c)/2
		s = p*(p-a)*(p-b)*(p-c)
		return s


a = Solution()
print a.largestTriangleArea([[0,0],[0,1],[1,0],[0,2],[2,0]])
