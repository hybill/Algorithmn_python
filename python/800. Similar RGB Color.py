import sys
class Solution(object):
	def similarRGB(self, color):
		"""
		:type color: str
		:rtype: str
		"""
		return '#'+self.findclost(color[1:3]) +self.findclost(color[3:5]) +self.findclost(color[5:7])
	def findclost(self,hex):
		A = int(hex,16)
		d = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
		Min = sys.maxint
		ans = ''
		for i in d:
			B = int(i+i,16)
			m = (A-B)**2
			if m < Min:
				ans = i
				Min = m
		return ans+ans

a = Solution()
print a.similarRGB("#09f166")