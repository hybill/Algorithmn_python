import java.util.*;

/*
public class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}*/
public class Partition {
    public ListNode partition(ListNode pHead, int x) {
        // write code here 
        if (pHead == null || pHead.next == null){
        	return  pHead;
        }
        ListNode p = pHead;
        ListNode Bx = new ListNode(-1);
        ListNode Lx = new ListNode(-1);
        ListNode Bxp = Bx;
        ListNode Lxp = Lx;
        while (p != null){
        	if (p.val >= x){
        		Bxp.next = new ListNode(p.val);
                Bxp = Bxp.next;
        	}else{
        		Lxp.next = new ListNode(p.val);
                Lxp = Lxp.next;
        	}
        	p = p.next;

        }
        p = Lx;
        while (p.next != null && p.next.val != -1){
        	p = p.next;
        }
        p.next = Bx.next;
        return  Lx.next;
    }
}