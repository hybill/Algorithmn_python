import java.util.*;

public class Clearer {
    public int[][] clearZero(int[][] mat, int n) {
        // write code here
        HashSet xS = new HashSet();
        HashSet yS = new HashSet();
        for (int x = 0; x < n; x++) {
        	for (int y = 0; y < n; y++){
        		if (mat[x][y] == 0){
        			xS.add(x);
        			yS.add(y);
        		}

        	}
        }
         for (int x = 0; x < n; x++) {
        	for (int y = 0; y < n; y++){
        		if (xS.contains(x) || yS.contains(y)){
        			mat[x][y] = 0;
        		}
        	}
        }
        return mat;
    }
}