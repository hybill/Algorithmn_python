#!/usr/bin/python
class Solution(object):
	def climbStairs(self, n):
		"""
		:type n: int
		:rtype: int
		"""
		a = [1,2]
		if n <=2:
			return a[n-1]
		x1 = 1
		x2 = 2
		for i in range(2,n):
			x = x1 + x2
			a.append(x)
			x1 = x2
			x2 = x
		return a[n-1]

a = Solution()
print a.climbStairs(3)