/*
public class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}*/
public class Solution {
    public ListNode ReverseList(ListNode head) {
    	ListNode res = null;
    	ListNode p = null;
    	while (head != null){
    		p = head.next;
    		head.next = res;
    		res = head;
    		head = p;

    	}
    	return res;

    }
}