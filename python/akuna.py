class MarkingPositionMonitor:
	def __init__(self):
		self.position = 0
		self.orders = {'FILL':0}
		self.orderlist = []
		self.cancellist = set()
		
	def on_event(self, message):
		import json
		D = json.loads(message)
		if not D.get('type'):
			print('not type error')
		else:
			Type = D['type']
			if Type == 'NEW':
				self.NEW(D)
			elif Type == 'ORDER_ACK':
				self.NEW(D)
			elif Type == 'ORDER_REJECT':
				pass
			elif Type == 'CANCEL':
				pass
			elif Type == 'CANCEL_ACK':
				self.CANCEL_ACK()
			elif Type == 'CANCEL_REJECT':
				pass
			elif Type == 'FILL':
				pass
		return sum(self.orders.values())
			
	def NEW(self,D):
		if D.get('side') and D.get('quantity'):
			if D['side'] == 'BUY':
				if D.get('order_id'):
					if int(D['quantity']) <= sum(self.orders.values()):
						self.orders[D['order_id']] = int(D['quantity'])
			elif D['side'] == 'SELL':
				if D.get('order_id'):
					self.orders[D['order_id']] = int(D['quantity']) * -1
			else:
				print('error')
		return self.position
	
	def ORDER_ACK(self,D):
		return self.position
	
	def ORDER_REJECT(self,D):
		if D.get('order_id') and self.orders.get(D['order_id']):
			del self.orders[D['order_id']]
	
	def CANCEL(self,D):
		if D.get('order_id'):
			self.cancellist.add(D['order_id'])
				
	def CANCEL_ACK(self,D):
		if D.get('order_id'):
			if D['order_id'] in self.cancellist:
				if self.orders.get(D['order_id']):
					del self.orders[D['order_id']]
					self.cancellist.remove(D['order_id'])
	
	def FILL(self,D):
		if self.orders.get('FILL'):
			self.orders['FILL'] += 
			
a = MarkingPositionMonitor
