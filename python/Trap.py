class Solution(object):
	def trap(self, height):
		"""
		:type height: List[int]
		:rtype: int
		"""
		if not height:
			return 0
		l = len(height)
		a = [0 for _ in range(l)]
		m = 0
		for i in range(l):
			m = max(m, height[i])
			a[i] = m
		Max = a[-1]
		last = 0
		j = l-1
		while j > 0 and a[j] == Max :
			last = max(height[j], last)
			a[j] = last
			j -= 1
		return sum([a[i]-height[i] for i in range(l)])	



a = Solution()
print a.trap([0,1,0,2,1,0,1,3,2,1,2,1])