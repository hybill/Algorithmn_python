import java.util.*;
 
/*
public class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;
    public TreeNode(int val) {
        this.val = val;
    }
}*/
 
public class Successor {
    //前一个节点，令此引用按照中序遍历的来移动，利用它来进行逆向移动
    //即原来只能是  根→左  现在可以  左→根
    private TreeNode pre = new TreeNode(-1);
      
    public int findSucc(TreeNode root, int p) {
        //空则表示没有
        if (root == null)
            return -1;
         
        //一路向左，到达最左边的叶子
        int left = findSucc(root.left, p);
             
        if (left == -1) {
        //最开始到达最左节点后，令pre指向该节点，同时root为pre的后继节点
        ////其后按照左根右的顺序移动
             
            //如果上一个节点中找到p，则该节点后其后继节点
            if (pre.val == p) {
                return root.val;
            }                
            pre = root; //如果前一个节点不是，则pre指向root的对象，
            return findSucc(root.right, p);               
        }     
             
        //如果找不到则返回上一层的根节点，方向为 左→根
        return left;
    }
}