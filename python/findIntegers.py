class Solution(object):
	def findIntegers(self, num):
		self.c = 1
		def f(n,lastIsone):
			if n <= num:
				self.c += 1
				a = n+n
				if lastIsone:
					f(a,False)
				else:
					f(a,False)
					f(a+1,True)
		if num<=1:
			return num+1
		else:
			f(1,True)
		return self.c
		
a = Solution()
print a.findIntegers(999999999)