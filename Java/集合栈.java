import java.util.*;

public class SetOfStacks {
    public ArrayList<ArrayList<Integer>> setOfStacks(int[][] ope, int size) {
        // write code here
        ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
        ArrayList<Integer> cur = new ArrayList<Integer>(size);
        list.add(cur);
        for (int i = 0; i<ope.length;i++){
        	if (ope[i][0] == 1){
        		if (cur.size()<size){
        			cur.add(ope[i][1]);
        		}else{
        			cur = new ArrayList<Integer>(size);
        			list.add(cur);
        			cur.add(ope[i][1]);
        		}
        	}else if(ope[i][0] == 2){
        		if (cur.size() >0){
        			cur.remove(cur.size()-1);
        		}else{
        			list.remove(list.size()-1);
        			cur = list.get(list.size()-1);
        			cur.remove(cur.size()-1);
        		}

        	}
        }
        return list;

    }
}