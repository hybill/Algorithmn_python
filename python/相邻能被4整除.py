#!/usr/bin/python
try:
	t = int(raw_input())
	for i in range(t):
		n = raw_input()
		ns = map(int, raw_input().split(' '))
		dan = 0
		d2 = 0
		d4 = 0
		for x in ns:
			if x%4 == 0:
				d4 += 1
			else:
				if x%2 == 1:
					dan += 1
				else:
					d2 += 1
		if d2 >0:
			d4 -= 1
		if dan-d4<=1 :
			print 'Yes'
		else:
			print 'No'
except:
	pass
