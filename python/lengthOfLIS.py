#!/usr/bin/python
class Solution(object):
	def lengthOfLIS(self, nums):
		"""
		:type nums: List[int]
		:rtype: int
		"""
		if not nums:
			return 0
		tail = [1]* len(nums)
		MAX = 0
		for i in range(0,len(nums)):
			n = nums[i]
			for j in range(0,i):
				if nums[j] < n:
					tail[i] = max(tail[j]+1,tail[i])
			MAX = max(tail[i],MAX)
		return MAX

s = Solution()
print s.lengthOfLIS([10,9,2,5,3,7,101,18])