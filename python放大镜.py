 # -*- coding: utf-8 -*

import tkinter
from PIL import ImageGrab,ImageTk

#create window
root = tkinter.Tk()
screenWidth = root.winfo_screenwidth()
screenHeight = root.winfo_screenheight()
root.geometry(str(1000)+'x'+str(1000)+'+0+0')
root.overrideredirect(True)
#lock the frame of  window
root.resizable(False,False)
#创建画布，显示全屏截图
canvas = tkinter.Canvas(root,bg='white',width=screenWidth,height=screenHeight)
image = ImageTk.PhotoImage(ImageGrab.grab())
canvas.create_image(screenWidth//2, screenHeight//2,image = image)
#右键退出程序
def onMouseRightClick(event):
	root.destroy()
canvas.bind('<Button-3>',onMouseRightClick)

#截图窗口半径
radius = 20
def onMouseMove(event):
	global lastIm
	try:
		canvas.delete(lastIm)
	except:
		pass
	#获取鼠标位置
	x = event.x
	y = event.y
	#二次截图
	subIm = ImageGrab.grab((x-radius,y-radius,x+radius,y+radius))
	subIm = subIm.resize((radius*6,radius*6))
	subIm = ImageTk.PhotoImage(subIm)
	lastIm = canvas.create_image(x-70,y-70, image=subIm)
	canvas.update()

#绑定鼠标移动事件处理函数
canvas.bind('<Motion>',onMouseMove)
#把画布对象canvas放置到窗体上
canvas.pack(fill=tkinter.BOTH,expand = tkinter.YES)

root.mainloop()