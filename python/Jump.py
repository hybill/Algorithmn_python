#!/usr/bin/python
class Solution(object):		
	def canCross(self, stones):
		"""
		:type stones: List[int]
		:rtype: bool
		"""
		def check(stones,target,p,k):
			if p == target:
				return True
			elif p > target:
				return False
			if (p,k) in self.state:
				return False
			else:
				self.state.add((p,k))
			if p in stones:
				if k>1:
					return check(stones,target,p+k+1,k+1) or check(stones,target,p+k,k) or check(stones,target,p+k-1,k-1)
				else:
					return check(stones,target,p+k+1,k+1) or check(stones, target, p+k, k)
			else:
				return False
		self.state = set()
		target = stones[-1]
		stones = set(stones)
		return check(stones,target,1,1)

a = Solution()
#print a.canCross([0,1,3,5,6,8,12,17])
print a.canCross([0,1,2,3,4,8,9,11])
#print a.canCross([0,2])
#print a.canCross([0,1])