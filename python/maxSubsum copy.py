def maxSubsum(nums):
	curSum = maxSum = 0
	for num in nums:
		curSum = max(curSum+num,num)
		maxSum = max(curSum,maxSum)
	return maxSum
a = [1,3,4,54,-56,6,4,3,2,3,4]
print maxSubsum(a)