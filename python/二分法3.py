#!/usr/bin/python
try:
	nums = [1,1,2,3,3]
	l = len(nums)
	start = 0
	end = l-1
	while start<end:
		mid = (start+end)>>1
		if mid < l-1 and nums[mid-1]<nums[mid]<nums[mid+1]:
			break
		if nums[mid-1] == nums[mid]:
			if (mid-start)%2 == 0:
				end = mid -2
			else:
				start = mid + 1
		else:
			if (mid-start)%2 == 0:
				start = mid 
			else:
				end = mid -1 
	print '2=',nums[end]	
		
except EOFError:
	print EOFError
