#!/usr/bin/python
class Solution(object):
	def eventualSafeNodes(self, graph):
		"""
		:type graph: List[List[int]]
		:rtype: List[int]
		"""
		ans = []
		l = len(graph)
		checklist = [False] * l
		safelist = [False] * l
		for i in xrange(l):
			if checklist[i] is False:
				self.DFS(i, graph, [i],checklist,safelist)
		for i in xrange(l):
			if checklist[i] is False:
				ans.append(i)
		return ans
			
	def DFS(self,node,graph,path,checklist,safelist):
		if graph[node]:
			
				for i in graph[node]:
			if safelist[i]:
				if i in path or checklist[i]:
					for j in path:
						checklist[j] = True
				else:
					newpath = path[:]
					newpath.append(i)
					self.DFS(i, graph, newpath, checklist)
		
graph = [[1,2],[2,3],[5],[0],[5],[],[]]
a = Solution()
print a.eventualSafeNodes(graph)