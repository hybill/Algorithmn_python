#!/usr/bin/python

def quickSort(nums):
	if not nums:
		return nums
	p = nums[0]
	right = []
	left = []
	for i in nums[1:]:
		if i < p:
			left.append(i)
		else:
			right.append(i)
	return quickSort(left)+[p]+quickSort(right)

def MergSort(nums):
	if not nums:
		return nums
	mid = len(nums)//2
	left = nums[:mid]
	right = nums[mid:]
	if len(left) > 1:
		left = MergSort(left)
	if len(right) > 1:
		right = MergSort(right)
	ans = []
	while left and right:
		if left[-1] > right[-1]:
			ans.append(left.pop())
		else:
			ans.append(right.pop())
	ans.reverse()
	return (left or right ) + ans


def MaxHeap(heap,size,root):
	left = 2*root + 1
	right = left + 1
	larger = root
	if left < size and heap[larger] < heap[left]:
		larger = left
	if right < size and heap[larger]< heap[right]:
		larger = right
	if larger != root:
		heap[larger],heap[root] = heap[root],heap[larger]
		MaxHeap(heap, size, larger)

def buildheap(nums):
	l = len(nums)
	for i in xrange((l-2)//2,-1,-1):
		nums[0],nums[i] = nums[i],nums[0]
		MaxHeap(nums,l,i)
	return nums			


print quickSort([2,3,4,2,12,2,4,2,34,42,2,1])
print MergSort([2,3,4,2,12,2,9,5,4,2,34,42,2,1])
print buildheap([2,3,4,2,12,2,9,5,4,2,34,42,2,1])
