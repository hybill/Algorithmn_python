#!/usr/bin/python
class Solution(object):
	def fractionToDecimal(self, numerator, denominator):
		"""
		:type numerator: int
		:type denominator: int
		:rtype: str
		"""
		singnal = ''
		if numerator*denominator<0:
			singnal = '-'
		numerator = abs(numerator)
		denominator = abs(denominator)
		a = numerator/denominator
		b = numerator%denominator
		if b == 0:
			return singnal+a
		f1 = a
		f2 = []
		table = {}
		p = 0
		repeatstart = ''
		while b != 0:
			remider = b*10
			a = remider/denominator
			b = remider%denominator
			if table.get(a):
				if b in table[a]:
					repeatstart = a
					p = table[a].index(b)
					b = 0
				else:
					table[a].append(b)
					f2.append(a)
			else:
				table[a] = [b]
				f2.append(a)
		
		s2 = ''
		if p == 0 and repeatstart == '':
			for x in f2:
				s2 += str(x)
			return singnal+str(f1)+'.'+s2
		else:
			for x in f2:
				if x == repeatstart:
					if p == 0:
						s2 = s2+'('+str(x)
						p = -1
					else:
						s2 += str(x)
						p -= 1
				else:
					s2 += str(x)
			return singnal+str(f1)+'.'+s2+')'
		
a = Solution()
print a.fractionToDecimal(-1, 2)