class Solution(object):
	def isPalindrome(self, x):
		"""
		:type x: int
		:rtype: bool
		"""
		if x == 0:
			return True
		if x > 0:
			s = str(x)
			if s == s[::-1]:
				return True
			else:
				return False
		if x < 0:
			return False
		