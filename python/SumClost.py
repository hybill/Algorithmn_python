#!/usr/bin/python
import sys
class Solution(object):
	def threeSumClosest(self, nums, target):
		clo = nums[0]+nums[1]+nums[2]
		nums = sorted(nums)
		for i in range(len(nums)-2):
			l = i+1
			r = len(nums) - 1
			if i>0 and nums[i] == nums[i-1]:
				continue
			while l < r:
				s = nums[i]+nums[r]+nums[l]
				if s == target:
					return target
				elif s<target:
					l += 1
				elif s> target:
					r -= 1
				if abs(s-target) < abs(clo-target):
					clo = s			
		return clo		

a = Solution()
print a.threeSumClosest([-1,2,1,-4],1)