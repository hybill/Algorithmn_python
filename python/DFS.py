#!/usr/bin/python
class Solution(object):
	def kthSmallest(self, root, k):
		"""
		:type root: TreeNode
		:type k: int
		:rtype: int
		"""
		count = []
		self.DFS(root,count)
		return count[k-1]
	
	def DFS(node,count):
		if not node:
			return
		self.DFS(root.left,count)
		count.append(root.val)
		self.DFS(root.right,count)