#!/usr/bin/python
class Solution:
	def GetUglyNumber_Solution(self, index):
		if index == 0:
			return None
		ugly = [1]
		i2,i3,i5 = 0,0,0
		while index > 1:
			u2,u3,u5 = 2 * ugly[i2],3*ugly[i3],5*ugly[i5]
			umin = min((u2,u3,u5))
			if umin == u2:
				i2 += 1
			if umin == u3:
				i3 += 1
			if umin == u5:
				i5 += 1
			ugly.append(umin)
			index -= 1
		return ugly[-1]
		
a = Solution()
print a.GetUglyNumber_Solution(10)