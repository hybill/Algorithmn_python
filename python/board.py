#!/usr/bin/python
class Solution(object):
	def solve(self, board):
		if board:
			checkl = []
			lx = len(board)
			ly = len(board[0])
			for i in range(lx):
				for j in range(ly):
					if board[i][j] == 'O':
						if i == 0 or j == 0 or i == lx -1 or j == ly -1:
							checkl.append((i,j))
						else:
							board[i][j] = 'S'
			while checkl:
				new = []		
				for (x,y) in checkl:
					if x > 1:
						if board[x-1][y] == 'S':
							new.append((x-1,y))
							board[x-1][y] = 'O'
					if y > 1:
						if board[x][y-1] == 'S':
							new.append((x,y-1))
							board[x][y-1] = 'O'
					if x < lx - 2:
						if board[x+1][y] == 'S':
							new.append((x+1,y))
							board[x+1][y] = 'O'
					if y < ly - 2:
						if board[x][y+1] == 'S':
							new.append((x,y+1))
							board[x][y+1] = 'O'
				checkl = new	
			for i in range(lx):
				for j in range(ly):
					if board[i][j] == 'S':
						board[i][j] = 'X'	
a = Solution()
a.solve([["O","O","O","O","X","X"],["O","O","O","O","O","O"],["O","X","O","X","O","O"],["O","X","O","O","X","O"],["O","X","O","X","O","O"],["O","X","O","O","O","O"]])