#!/usr/bin/python

class Solution:
	"""
	@param arr: The 2-dimension array
	@return: Return the column the leftmost one is located
	"""
	def getColumn(self, arr):
		# Write your code here
		Min = 1001
		column = 0
		for line in xrange(len(arr)):
			if 1 in arr[line]:
				index = arr[line].index(1)
				if index < Min:
					Min = index
					column = line
		return Min
		
a = Solution()
print a.getColumn([[0,0,0,1],[1,1,1,1]])