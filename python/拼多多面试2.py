#!/usr/bin/python
n = int(raw_input())
ns = map(int, raw_input().split(' '))
d = {1:0,2:0,3:0,4:0}
for i in ns:
	d[i] += 1
ans = d[4]
ans += d[3]
if d[1] >= d[3]:
	d[1] -= d[3]
else:
	d[1] = 0
ans += d[2]//2
if d[2]%2 != 0:
	if d[1]	>= 2:
		d[1] -= 2
		ans += 1
	else:
		d[1] = 0
		ans += 1
ans += d[1]//4
if d[1]%4 != 0:
	ans += 1
print(ans)