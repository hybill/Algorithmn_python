#!/usr/bin/python

n = raw_input()
ns= map(int, raw_input().split(' '))
even = 0
odd = 0
for i in ns:
	even += i/2
	odd += i%2
if odd > 0:
	print(even/odd*2+1)
else:
	print(even*2)