#!/usr/bin/python
class Solution(object):
	def addBinary(self, a, b):
		"""
		:type a: str
		:type b: str
		:rtype: str
		"""
		a1 = map(int,list(a))
		b1 = map(int,list(b))
		ans = []
		progress = 0
		while a1 and b1:
			a2 = a1.pop()
			b2 = b1.pop()
			s = a2+b2+progress
			ans.append((s)%2)
			progress = s//2
		while a1:
			a2 = a1.pop()
			s = progress+a2
			ans.append((s)%2)
			progress = s//2
		while b1:
			b2 = b1.pop()
			s = progress+b2
			ans.append((s)%2)
			progress = s//2
		if progress == 1:
			ans.append(1)
		rs =''
		for i in ans[::-1]:
			rs += str(i)
		return rs

a = Solution()
print a.addBinary('11', '1')	