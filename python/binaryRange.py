#!/usr/bin/python
class Solution(object):
	def searchRange(self, nums, target):
		"""
		:type nums: List[int]
		:type target: int
		:rtype: List[int]
		"""
		if not nums:
			return [-1,-1]
		length = len(nums)
		left = 0
		right = length-1
		mid = 0
		while left<right:
			mid = (left+right)>>1
			if nums[mid] == target:
				break
			elif nums[mid] < target:
				left = mid
			else:
				right = mid
		if nums[mid] != target:
			return [-1,-1]
		else:
			l = mid
			r = mid
			while r+1 < length and nums[r+1] == target:
				r += 1
			while l-1 >= 0 and nums[l-1] == target:
				l -= 1
		return [l,r]

a = Solution()
#print a.searchRange([5,7,7,8,8,10], 8)
#print a.searchRange([2,2], 2)
#print a.searchRange([1,2], 1)
print a.searchRange([1,2], 2)