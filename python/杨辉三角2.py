#!/usr/bin/python
class Solution(object):
	def generate(self, numRows):
		"""
		:type numRows: int
		:rtype: List[List[int]]
		"""
		if numRows == 0:
			return []
		if numRows == 1:
			return [[1]]
		ans = [[1]]
		s = [1,1]
		while numRows > 1:
			ans.append(s)
			numRows -= 1
			a = [1]
			for i in range(0,len(s)-1):
				a.append(s[i]+s[i+1])
			a.append(1)
			s = a
		return ans
		

a = Solution()
print a.generate(5)
