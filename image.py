from PIL import Image
import numpy as np
import numpy
import matplotlib.pyplot as plt
numpy.set_printoptions(threshold=numpy.nan)
class ImageOperation:
	def __init__(self,path):
		self.image = Image.open(path)
		self.imageData=np.array(self.image .convert('L'))
	
	def clearColor(self):
		rows,cols=self.imageData.shape
		for i in range(rows):
			for j in range(cols):
				if (self.imageData[i,j]<=100):
					self.imageData[i,j]=0
				else:
					self.imageData[i,j]=1
	def showMatrix(self):
		print self.imageData
		
	def show(self):		
		new_image = Image.fromarray(self.imageData)
		new_image.show()


path = 'test1.png'
a = ImageOperation(path)
#a.showMatrix()
a.clearColor()
a.showMatrix()
a.show()