import numpy as np
def createRandomArray(n):
	randomArray = np.random.randint(0,100,n)
	print randomArray
	return randomArray


def MergeSort(seq):
	mid = len(seq)//2
	LFT = seq[:mid]
	RIT = seq[mid:]
	if len(LFT)>1: LFT = MergeSort(LFT)
	if len(RIT)>1: RIT = MergeSort(RIT)
	res = []
	while LFT and RIT:
		if LFT[-1] > RIT[-1]:
			res.append(LFT.pop())
		else:
			res.append(RIT.pop())
	res.reverse()
	return (RIT or LFT) + res		

		
seq = list(createRandomArray(10))
print MergeSort(seq)	