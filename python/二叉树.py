#!/usr/bin/python
# Definition for a binary tree node.
class TreeNode(object):
	def __init__(self, x):
		self.val = x
		self.left = None
		self.right = None

class Solution(object):	
	def hasPathSum(self, root, sum):
		if root.val == sum and not root.left and not root.right:
			return True
		elif root.val < sum:
			if root.left:
				if self.hasPathSum(root.left,sum-root.val):
					return True
			if root.right:
				if self.hasPathSum(root.right,sum-root.val):
					return True
		return False