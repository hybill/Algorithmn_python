#!/usr/bin/python
class Solution(object):
	def escapeGhosts(self, ghosts, target):
		"""
		:type ghosts: List[List[int]]
		:type target: List[int]
		:rtype: bool
		"""
		ghostsSteps = []
		for i in ghosts:
			ghostsSteps.append(abs(i[0]-target[0])+abs(i[1]-target[1]))
		selfSteps = abs(abs(target[0])+abs(target[1]))
		for i in ghostsSteps:
			if i <= selfSteps:
				return False
		return True
			
a = Solution()
print a.escapeGhosts([[1, 0], [0, 3]], [0, 1])
print a.escapeGhosts([[2, 0]], [1, 0])

