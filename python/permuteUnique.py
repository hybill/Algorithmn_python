#!/usr/bin/python
class Solution(object):
	def permuteUnique(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		if not nums:
			return []
		nums.sort()
		rs = [[]]
		for n in nums:
			newrs = []
			l = len(rs[-1])
			for seq in rs:
				for i in xrange(l,-1,-1):
					if i < l and seq[i] == n:
						break
					newrs.append(seq[:i]+[n]+seq[i:])
			rs = newrs
		return rs
	
a = Solution()
print a.permuteUnique([1,2,3])
			
			
			