#!/usr/bin/python

class Parent:
	__race = 'asian'
	gender = 'male'
	_isBeautiful = False
	def __init__(self,name,age):
		print('parent init')
		self.name = name
		self.age = age
	
	def parentMethod1(self):
		print('parentMethod1')
		
	def _parentMethod2(self):
		print('parentMethod2')

class Child(Parent):
	def __init__(self):
		print('child init')
	
a = Child()
a.parentMethod1()
a.gender = 'female'
print(a.gender)
a._isBeautiful = True
print(a._isBeautiful)
a._parentMethod2()